

// tslint:disable-next-line:no-namespace
declare namespace Express {

	// tslint:disable-next-line:interface-name
	export interface Request {
		id: string;
		rawBody?: Buffer;
		auth: {
			sessionId?: string;
			username?: string | null;
		};
	}
}
