import { Entity } from '../../../domain/entity/Entity';
import { IEntityRepository } from '../../../domain/entity/IEntityRepository';
import { Db } from '../db';

export class EntityRepository implements IEntityRepository {
	constructor (
		private db: Db,
	) {}

	public async list (): Promise<Entity[]> {
		const result = await this.db.models.Entity.findAll();

		return result.map((item) => {
			return item.toEntity();
		});
	}

	public async getById (id: string): Promise<Entity | null> {
		const result = await this.db.models.Entity.findByPk(id);

		return result && result.toEntity();
	}

	public async save (entity: Entity): Promise<void> {
		await this.db.models.Entity.create(entity);
	}
}
