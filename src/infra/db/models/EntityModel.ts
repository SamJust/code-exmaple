import {Model, DataTypes, Sequelize} from 'sequelize';
import { Entity } from '../../../domain/entity/Entity';

export class EntityModel extends Model {
	public id!: string;
	public foo!: string;
	public bar!: number | null;

	static fromEntity (entity: Entity): EntityModel {
		return new EntityModel(entity);
	}

	public toEntity (): Entity {
		return new Entity(this);
	}

	public static initWith (sequlize: Sequelize) {
		EntityModel.init({
			id: {
				type:       DataTypes.UUID,
				primaryKey: true,
			},
			foo: {
				type: DataTypes.STRING,
			},
			bar: {
				type:      DataTypes.INTEGER,
				allowNull: true,
			},
			createdAt: {
				type:      DataTypes.DATE,
				allowNull: false,
			},
			updatedAt: {
				type:      DataTypes.DATE,
				allowNull: false,
			},
		}, {
			tableName: 'entities',
			sequelize: sequlize,
		});
	}

	public static initRelations (): void {
		// Do noting
	}
}
