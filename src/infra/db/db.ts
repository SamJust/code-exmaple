import { Config } from '../../app/Config';
import { IAsyncInit } from '../../types';
import {Sequelize} from 'sequelize';
import { Logger } from '../../app/Logger';
import { EntityModel } from './models/EntityModel';

export class Db implements IAsyncInit {
	private sequelize!: Sequelize;

	public models = {
		Entity: EntityModel,
	};

	constructor (
		private config: Config,
		private logger: Logger,
	) {}

	public async init () {
		const {
			host,
			port,
			user,
			pass,
			database,
		} = this.config.infra.db;

		this.sequelize = new Sequelize({
			host:     host,
			port:     port,
			username: user,
			password: pass,
			database: database,
			dialect:  'postgres',
		});

		Object.keys(this.models).forEach((model) => {
			(this.models as any)[model].initWith(this.sequelize);
		});

		try {
			this.logger.info('Conecting to DB');

			await this.sequelize.authenticate();

			this.logger.info('Connected to DB');
		} catch (error) {
			this.logger.error('Error conecting to DB', {
				error: error,
			});

			throw error;
		}
	}

	public async start () {
		const models = Object.values(this.models);

		for (const model of models) {
			model.initRelations();
		}
	}

	public async stop () {
		this.logger.info('Stopping DB conection');

		await this.sequelize.close();

		this.logger.info('Stopped DB connection');
	}
}
