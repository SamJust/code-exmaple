import 'reflect-metadata';
import {asClass, asValue, AwilixContainer, createContainer, InjectionMode} from 'awilix';
import {App} from '../app/App';
import {Logger} from '../app/Logger';
import {Config} from '../app/Config';
import {Web} from '../interfaces/web/Web';
import {Usage} from '../app/plugins/Usage';
import { EntityService } from '../domain/entity/EntityService';
import { Db } from '../infra/db/db';
import { EntityRepository } from '../infra/db/repositories/EntityRepository';


export class Container {

	public static create (): AwilixContainer {
		const container = createContainer({
			injectionMode: InjectionMode.CLASSIC,
		});

		container.register({
			container: asValue(container),

			// App
			app:    asClass(App).singleton(),
			config: asClass(Config).singleton(),
			logger: asClass(Logger).singleton(),
			usage:  asClass(Usage).singleton(),

			// Domain
			entityService: asClass(EntityService).singleton(),

			// Interfaces
			web: asClass(Web).singleton(),

			// Libs

			// Infrastructure
			db:               asClass(Db).singleton(),
			entityRepository: asClass(EntityRepository).singleton(),
		});

		return container;
	}

	private constructor () {
		// do nothing
	}
}
