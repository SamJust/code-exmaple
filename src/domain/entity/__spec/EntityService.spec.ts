import { asValue, createContainer } from 'awilix';
import sinon from 'sinon';
import {EntityRepository} from '../../../infra/db/repositories/EntityRepository';
import { Entity } from '../Entity';
import {EntityService} from '../EntityService';

describe('EntityService', () => {
	const container = createContainer();

	const repositoryStub = sinon.createStubInstance(EntityRepository);
	container.register({
		entityRepository: asValue(repositoryStub),
	});

	describe('showEntity()', () => {
		let instance: EntityService;

		beforeEach(() => {
			instance = container.build(EntityService);
		});

		afterEach(() => {
			sinon.resetBehavior();
		});

		it('should return entity', async () => {
			const entity = new Entity({
				id:  'AN ID',
				foo: 'foo',
				bar: 322,
			});
			repositoryStub.getById.resolves(entity);

			const result = await instance.showEntity('AN ID');

			expect(result).toEqual({
				id:  'AN ID',
				foo: 'foo',
				bar: 322,
			});
		});
	});
});

// THESE ARE STUBS FOR TESTS
// THESE ARE NOT EVEN RUN ONCE

