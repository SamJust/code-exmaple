import { Entity } from './Entity';

export interface IEntityRepository {
	getById (id: string): Promise<Entity | null>;
	list (): Promise<Entity[]>;
	save (entity: Entity): Promise<void>;
}
