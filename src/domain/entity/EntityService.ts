import { AppError } from '../../app/AppError';
import { Entity } from './Entity';
import { IEntityRepository } from './IEntityRepository';

export class EntityService {
	constructor (
		private entityRepository: IEntityRepository,
	) {}

	public async listEntities (): Promise<Entity[]> {
		const result = await this.entityRepository.list();

		for (const entity of result) {
			if (entity.bar) {
				entity.bar = entity.bar * 1.25; // Some awesome bussiness logic here, mkay?
			}
		}

		return result;
	}

	public async showEntity (id: string): Promise<Entity> {
		const result = await this.entityRepository.getById(id);

		if (!result) {
			throw new AppError('INVALID ID', 'No entity with such id found');
		}

		return result;
	}

	public async createEntity (foo: string, bar: number | null): Promise<Entity> {
		const entity = new Entity({
			foo: foo,
			bar: bar,
		});

		await this.entityRepository.save(entity);

		return entity;
	}
}
