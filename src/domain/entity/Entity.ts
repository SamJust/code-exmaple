import uuid from 'uuid';

export class Entity {
	public id: string;
	public foo: string;
	public bar: number | null;

	constructor (init: Partial<Entity>) {
		this.id = init.id || uuid.v4();
		this.foo = init.foo!;
		this.bar = init.bar || null;
	}
}
