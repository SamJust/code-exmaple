import {Express, Request, Response} from 'express';
import {WebError} from '../WebError';
import { AppError } from '../../../app/AppError';


export function registerErrorHandler (app: Express): void {
	// It is need to be the last middleware !!!

	app.use((req, res, next) => { // 404
		return next(new WebError(404));
	});

	app.use((err: Error, req: Request, res: Response, next: () => void) => {
		if (err instanceof WebError) {
			app.locals.logger.warn(err.toString());
			res.status(400).json(err.toJSON());
			return;
		}

		if (err instanceof AppError) {
			app.locals.logger.error(err.toString());
			res.status(400).json(err.toJSON());
			return;
		}

		res.status(400).end(err.message);
	});
}
