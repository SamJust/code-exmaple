import {Express} from 'express';
import {AwilixContainer} from 'awilix';
import {processControllers} from './controllers-helper';
import {registerSwagger} from './swagger-helper';
import {AuthType} from '../auth/auth.types';
import {NoneAuth} from '../auth/NoneAuth';

/* Controllers */
import { ProbeController } from '../controllers/ProbeController';
import { PingController } from '../controllers/PingController';
import { EntityController } from '../controllers/EntityController';

export function registerControllers (app: Express, container: AwilixContainer): void {
	app.locals.auth = {
		[AuthType.None]: container.build(NoneAuth),
	};

	app.locals.controllers = [
		container.build(ProbeController),
		container.build(PingController),
		container.build(EntityController),
	];

	processControllers(app);
	registerSwagger(app);
}
