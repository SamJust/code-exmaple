import joi from '../../../app/Validator';
import {handler} from '../decorators';
import {AuthType} from '../auth/auth.types';
import { EntityService } from '../../../domain/entity/EntityService';
import {Request} from 'express';


export class EntityController {
	constructor (
		private entityService: EntityService,
	) {}

	@handler({
		description: 'Get entity by id',
		method:      'GET',
		path:        '/entity/:entityId',
		auth:        AuthType.None,
		validate:    {
			params: {
				entityId: joi.string().guid().required(),
			},
		},
		response: {
			200: joi.object().keys({
				data: joi.object().keys({
					id:  joi.string(),
					foo: joi.string(),
					bar: joi.number().allow(null),
				}),
			}),
		},
	})
	public async showEntity (req: Request): Promise<object> {
		const result = await this.entityService.showEntity(req.params.entityId);

		return {
			id:  result.id,
			foo: result.foo,
			bar: result.bar,
		};
	}

	@handler({
		description: 'Create entity',
		method:      'POST',
		path:        '/entity',
		auth:        AuthType.None,
		validate:    {
			body: {
				foo: joi.string().required(),
				bar: joi.number(),
			},
		},
		response: {
			200: joi.object().keys({
				data: joi.object().keys({
					id:  joi.string(),
					foo: joi.string(),
					bar: joi.number().allow(null),
				}),
			}),
		},
	})
	public async createEntity (req: Request): Promise<object> {
		const result = await this.entityService.createEntity(req.body.foo, req.body.bar);

		return {
			id:  result.id,
			foo: result.foo,
			bar: result.bar,
		};
	}

	@handler({
		description: 'List entities',
		method:      'GET',
		path:        '/entities',
		auth:        AuthType.None,
		validate:    {},
		response:    {
			200: joi.object().keys({
				data: joi.array().items(joi.object().keys({
					id:  joi.string(),
					foo: joi.string(),
					bar: joi.number().allow(null),
				})),
			}),
		},
	})
	public async listEntities (req: Request): Promise<object> {
		const result = await this.entityService.listEntities();

		return result.map((entity) => {
			return {
				id:  entity.id,
				foo: entity.foo,
				bar: entity.bar,
			};
		});
	}
}
