import {Request} from 'express';


export const enum AuthType {
	None = 'None', // Some extra auth logic might come here. Very sophisticated
}

export interface IAuth {
	credentials (req: Request): Promise<any>;
	auth (req: Request, credentials: any): Promise<Request['auth']>;
}
