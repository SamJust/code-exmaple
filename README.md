# Code exmpale

## Using `yarn`
In this application we using `yarn` as package manager.
To install it follow [yarn installation](https://classic.yarnpkg.com/en/docs/install/)

Or for Homebrew:
```bash
brew install yarn
```


## Install dependacies
```bash
yarn
```
## Run app for dev purposes

```bash
yarn start:dev
```

