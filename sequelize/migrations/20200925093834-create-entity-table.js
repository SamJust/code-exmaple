module.exports = {
	up: async (queryInterface, DataTypes) => {
		await queryInterface.createTable('entities', {
			id: {
				type:       DataTypes.UUID,
				primaryKey: true,
			},
			foo: {
				type: DataTypes.STRING,
			},
			bar: {
				type:      DataTypes.INTEGER,
				allowNull: true,
			},
			createdAt: {
				type:      DataTypes.DATE,
				allowNull: false,
			},
			updatedAt: {
				type:      DataTypes.DATE,
				allowNull: false,
			},
		});
	},

	down: async (queryInterface, Sequelize) => {
		await queryInterface.dropTable('entities');
	},
};
